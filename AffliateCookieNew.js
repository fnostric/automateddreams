/*!
 * jQuery Cookie Plugin v1.4.1
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2006, 2014 Klaus Hartl
 * Released under the MIT license
 */
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
(function (factory) {
   if (typeof define === 'function' && define.amd) {
      // AMD (Register as an anonymous module)
      define(['jquery'], factory);
   } else if (typeof exports === 'object') {
      // Node/CommonJS
      module.exports = factory(require('jquery'));
   } else {
      // Browser globals
      factory(jQuery);
   }
}(function ($) {

   var pluses = /\+/g;

   function encode(s) {
      return config.raw ? s : encodeURIComponent(s);
   }

   function decode(s) {
      return config.raw ? s : decodeURIComponent(s);
   }

   function stringifyCookieValue(value) {
      return encode(config.json ? JSON.stringify(value) : String(value));
   }

   function parseCookieValue(s) {
      if (s.indexOf('"') === 0) {
         // This is a quoted cookie as according to RFC2068, unescape...
         s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
      }

      try {
         // Replace server-side written pluses with spaces.
         // If we can't decode the cookie, ignore it, it's unusable.
         // If we can't parse the cookie, ignore it, it's unusable.
         s = decodeURIComponent(s.replace(pluses, ' '));
         return config.json ? JSON.parse(s) : s;
      } catch(e) {}
   }

   function read(s, converter) {
      var value = config.raw ? s : parseCookieValue(s);
      return $.isFunction(converter) ? converter(value) : value;
   }

   var config = $.cookie = function (key, value, options) {

      // Write

      if (arguments.length > 1 && !$.isFunction(value)) {
         options = $.extend({}, config.defaults, options);

         if (typeof options.expires === 'number') {
            var days = options.expires, t = options.expires = new Date();
            t.setMilliseconds(t.getMilliseconds() + days * 864e+5);
         }

         return (document.cookie = [
            encode(key), '=', stringifyCookieValue(value),
            options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
            options.path    ? '; path=' + options.path : '',
            options.domain  ? '; domain=' + options.domain : '',
            options.secure  ? '; secure' : ''
         ].join(''));
      }

      // Read

      var result = key ? undefined : {},
         // To prevent the for loop in the first place assign an empty array
         // in case there are no cookies at all. Also prevents odd result when
         // calling $.cookie().
         cookies = document.cookie ? document.cookie.split('; ') : [],
         i = 0,
         l = cookies.length;

      for (; i < l; i++) {
         var parts = cookies[i].split('='),
            name = decode(parts.shift()),
            cookie = parts.join('=');

         if (key === name) {
            // If second argument (value) is a function it's a converter...
            result = read(cookie, value);
            break;
         }

         // Prevent storing a cookie that we couldn't decode.
         if (!key && (cookie = read(cookie)) !== undefined) {
            result[name] = cookie;
         }
      }

      return result;
   };

   config.defaults = {};

   $.removeCookie = function (key, options) {
      // Must not alter options, thus extending a fresh object...
      $.cookie(key, '', $.extend({}, options, { expires: -1 }));
      return !$.cookie(key);
   };

}));
(function ($) {
    console.log($);
    $(function() {
        console.log("ready!");
        var $affliate_link = '';
        var $affid = getParameterByName('affid', '');
        var $offer_id = getParameterByName('offer_id', '');
        var $transaction_id = getParameterByName('transaction_id', '');
        var $url_id = getParameterByName('url_id', '');

        if ($affid && !$.cookie('ad_affid')) {
            $.cookie('ad_affid', $affid, {expires: 30, path: '/', domain: '.holdenqigong.com', secure: true});

            if ($offer_id && !$.cookie('ad_offer_id')) {
                $.cookie('ad_offer_id', $offer_id, {expires: 30, path: '/', domain: '.holdenqigong.com', secure: true});
            }
            if ($url_id && !$.cookie('ad_url_id')) {
                $.cookie('ad_url_id', $url_id, {expires: 30, path: '/', domain: '.holdenqigong.com', secure: true});
            }
            if ($transaction_id && !$.cookie('ad_transaction_id')) {
                $.cookie('ad_transaction_id', $transaction_id, {
                    expires: 30,
                    path: '/',
                    domain: '.holdenqigong.com',
                    secure: true
                });
            }
        }else{
            $affid = $.cookie('ad_affid');
        }
        if ($affid) {
            var $affliate_link = 'affid=' + $.cookie('ad_affid');
            if ($.cookie('ad_offer_id')) var $affliate_link = $affliate_link + '&offer_id=' + $.cookie('ad_offer_id');
            if ($.cookie('ad_url_id')) var $affliate_link = $affliate_link + '&url_id=' + $.cookie('ad_url_id');
            if ($.cookie('ad_transaction_id')) var $affliate_link = $affliate_link + '&transaction_id=' + $.cookie('ad_transaction_id');
            console.log($affliate_link);
            if ($affliate_link) {
                $("a.elButton, .content a, #main-content a").each(function () {
                    var $this = $(this);
                    var $_href = $this.attr("href");
                    if(!$_href.split('#')[1] && $_href.indexOf('affid') < 0) {
                        $this.attr("href", $_href + ($_href.indexOf("?") < 0?'?':'&') + $affliate_link);
                    }
                });
            }
        }
    });
})(jQuery);